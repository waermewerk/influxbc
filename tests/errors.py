class TestError(Exception):
    """
    Base exception class for the test suite
    """


class TestSetupError(TestError):
    """
    Raised when test setup fails
    """
