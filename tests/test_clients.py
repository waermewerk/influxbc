import logging
import threading
import time

import influxdb as influxdb_client_v1
import influxdb_client as influxdb_client_v2
import numpy as np
from influxdb_client.client.write_api import SYNCHRONOUS
import requests.exceptions
import urllib3.exceptions

import pandas as pd
import pandas.testing
import pytest
import pytz

from src.influxbc.logging import LOGGER_NAME
from src.influxbc import (
    InfluxDBV1Client,
    InfluxDBV2Client,
    WriteMode,
    errors,
)
from src.influxbc.const import (
    INFLUX_EPOCH,
    INFLUX_V1_INT_DB,
    INFLUX_V2_INT_DB_MONIT,
    INFLUX_V2_INT_DB_TASKS,
)
from src.influxbc.typing import Client
from tests import (
    TEST,
    FOO,
    BAR,
    QUX,
    INFLUX_HOST,
    INFLUX_V1_PORT,
    INFLUX_V2_PORT,
    INFLUX_ORG,
    INFLUX_DB,
    INFLUX_USER,
    INFLUX_PW,
    INFLUX_TOKEN,
    DOES_NOT_EXIST,
)
from tests.errors import TestSetupError


# region Setup
@pytest.fixture(scope="function")
def db_init_v1() -> influxdb_client_v1.DataFrameClient:
    """
    Create a test DB and initial data in an InfluxDB V1
    """
    upstream_client = influxdb_client_v1.DataFrameClient(
        host=INFLUX_HOST,
        port=INFLUX_V1_PORT,
        username=INFLUX_USER,
        password=INFLUX_PW,
        database=INFLUX_DB,
    )
    with upstream_client:
        # Create test data in the DB
        test_df = TEST_DF.copy(deep=True)
        # Serialize the test data frame with the aid of upstream V2 client functionality
        try:
            upstream_client.write_points(
                dataframe=test_df,
                tags=TEST_TAGS,
                measurement=TEST_MEASUREMENT,
            )
        except requests.exceptions.ConnectionError as e:
            raise TestSetupError(
                "Could not connect to the configured InfluxDB V1: " + str(e)
            )
        yield upstream_client
        upstream_client.drop_database(INFLUX_DB)
        upstream_client.create_database(INFLUX_DB)


@pytest.fixture(scope="function")
def db_init_v2() -> influxdb_client_v2.InfluxDBClient:
    """
    Create a test DB and initial data in an InfluxDB V2
    """
    # noinspection HttpUrlsUsage
    upstream_client = influxdb_client_v2.InfluxDBClient(
        url=f"http://{INFLUX_HOST}:{INFLUX_V2_PORT}",
        token=INFLUX_TOKEN,
        org=INFLUX_ORG,
    )
    with upstream_client:
        # Create test data in the DB
        with upstream_client.write_api(write_options=SYNCHRONOUS) as write_api:
            test_df_with_tags = TEST_DF.copy(deep=True)
            test_df_with_tags = test_df_with_tags.assign(**TEST_TAGS)
            try:
                # noinspection PyTypeChecker
                write_api.write(
                    bucket=INFLUX_DB,
                    record=test_df_with_tags,
                    data_frame_measurement_name=TEST_MEASUREMENT,
                    data_frame_tag_columns=TEST_TAG_KEYS,
                )
            except urllib3.exceptions.NewConnectionError as e:
                raise TestSetupError(
                    "Could not connect to the configured InfluxDB V2: " + str(e)
                )
        yield upstream_client
        buckets_api = upstream_client.buckets_api()
        bucket = buckets_api.find_bucket_by_name(bucket_name=INFLUX_DB)
        buckets_api.delete_bucket(bucket=bucket)
        buckets_api.create_bucket(bucket=bucket)


@pytest.fixture(scope="function")
def client_v1() -> InfluxDBV1Client:
    """
    Create and yield a V1 client connected to an InfluxDB V1
    """
    # noinspection HttpUrlsUsage
    client = InfluxDBV1Client(
        host=INFLUX_HOST,
        port=INFLUX_V1_PORT,
        username=INFLUX_USER,
        password=INFLUX_PW,
        database=INFLUX_DB,
    )
    with client:
        yield client


@pytest.fixture(scope="function")
def client_v2() -> InfluxDBV2Client:
    """
    Create and yield a V2 client connected to an InfluxDB V2
    """
    # noinspection HttpUrlsUsage
    client = InfluxDBV2Client(
        url=f"http://{INFLUX_HOST}:{INFLUX_V2_PORT}",
        token=INFLUX_TOKEN,
        org=INFLUX_ORG,
        bucket_name=INFLUX_DB,
    )
    with client:
        yield client


VERSION_SETUPS_V2 = [
    (db_init_v2.__name__, client_v2.__name__),
]
VERSION_SETUPS_V1_V2 = [
    (db_init_v1.__name__, client_v1.__name__),
    (db_init_v2.__name__, client_v2.__name__),
]

# Test Data
TEST_MEASUREMENT = TEST
TEST_UTF_FIELD_KEY = "$ome.utf"
TEST_DATA = {FOO: (1, 2), BAR: (3, 4), TEST_UTF_FIELD_KEY: (7, 8)}
TEST_DATA_START = pd.Timestamp(2022, 12, 21, tzinfo=pytz.UTC)
TEST_DF = pd.DataFrame(
    data=TEST_DATA, index=pd.date_range(TEST_DATA_START, periods=2)
)
TEST_TAG_KEY_A = "a"
TEST_TAG_KEY_B = "b"
TEST_TAG_VALUE_A = "crux"
TEST_TAG_VALUE_B = "12"
TEST_TAGS = {
    TEST_TAG_KEY_A: TEST_TAG_VALUE_A,
    TEST_TAG_KEY_B: TEST_TAG_VALUE_B,
}
TEST_TAG_KEYS = tuple(TEST_TAGS.keys())
# endregion


# region Schema & Management
@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_get_databases(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test getting available databases from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # InfluxDB's internal databases
    all_internal_dbs = {
        1: [INFLUX_V1_INT_DB],
        2: [INFLUX_V2_INT_DB_MONIT, INFLUX_V2_INT_DB_TASKS],
    }

    # The client should return the single available test measurement
    internal_dbs = all_internal_dbs[client.client_major]
    assert set(client.get_databases()) == set([INFLUX_DB] + internal_dbs)


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V2)
def test_get_buckets(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test getting available buckets from the InfluxDB V2 client
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: InfluxDBV2Client = request.getfixturevalue(client_func)

    # The client should return the single available test measurement
    internal_dbs = [INFLUX_V2_INT_DB_MONIT, INFLUX_V2_INT_DB_TASKS]
    assert set(client.get_buckets()) == set([INFLUX_DB] + internal_dbs)


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_get_measurements(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test getting measurements from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # The client should return the single available test measurement
    assert client.get_measurements() == [TEST_MEASUREMENT]


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_get_fields(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test getting field keys from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # The client should return the field keys without a given measurement
    assert set(client.get_fields()) == {TEST_UTF_FIELD_KEY, BAR, FOO}

    # The client should return the field keys of a given measurement
    assert set(client.get_fields(measurement=TEST_MEASUREMENT)) == {
        TEST_UTF_FIELD_KEY,
        BAR,
        FOO,
    }

    # Write data to a second measurement
    df = pd.DataFrame(
        data={QUX: (1, 2)}, index=pd.date_range(TEST_DATA_START, periods=2)
    )
    client.write_data_frame(
        data_frame=df,
        measurement=FOO,
    )

    # The client should now return all field keys without a given measurement
    assert set(client.get_fields()) == {QUX, TEST_UTF_FIELD_KEY, BAR, FOO}

    # The client should still return the field keys of a given measurement
    assert set(client.get_fields(measurement=TEST_MEASUREMENT)) == {
        TEST_UTF_FIELD_KEY,
        BAR,
        FOO,
    }

    # The client should return empty data and raise a warning
    # when the given measurement does not exist
    with pytest.warns(UserWarning):
        assert len(client.get_fields(measurement=DOES_NOT_EXIST)) == 0


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_get_tag_keys(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test getting tag keys from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # The client should return the tag keys without a given measurement
    assert set(client.get_tag_keys()) == set(TEST_TAGS.keys())

    # The client should return the tag keys of a given measurement
    assert set(client.get_tag_keys(measurement=TEST_MEASUREMENT)) == set(
        TEST_TAGS.keys()
    )

    # Write data to a second measurement
    df = pd.DataFrame(
        data={QUX: (1, 2)}, index=pd.date_range(TEST_DATA_START, periods=2)
    )
    client.write_data_frame(data_frame=df, measurement=FOO, tags={FOO: FOO})

    # The client should now return all tag keys without a given measurement
    assert set(client.get_tag_keys()) == set(list(TEST_TAGS.keys()) + [FOO])

    # The client should still return the tag keys of a given measurement
    assert set(client.get_tag_keys(measurement=TEST_MEASUREMENT)) == set(
        TEST_TAGS.keys()
    )

    # The client should return empty data and raise a warning
    # when the given measurement does not exist
    with pytest.warns(UserWarning):
        assert len(client.get_tag_keys(measurement=DOES_NOT_EXIST)) == 0


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_get_tag_values(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test getting tag values from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # The client should return the tag values without a given measurement
    tag_keys = client.get_tag_values(tag_key=TEST_TAG_KEY_A)
    assert set(tag_keys) == {TEST_TAG_VALUE_A}

    # The client should return the tag values of a given measurement
    tag_keys = client.get_tag_values(
        tag_key=TEST_TAG_KEY_A, measurement=TEST_MEASUREMENT
    )
    assert set(tag_keys) == {TEST_TAG_VALUE_A}

    # Write data to a second measurement
    df = pd.DataFrame(
        data={QUX: (1, 2)}, index=pd.date_range(TEST_DATA_START, periods=2)
    )
    client.write_data_frame(
        data_frame=df, measurement=FOO, tags={TEST_TAG_KEY_A: FOO}
    )

    # The client should now return all tag values without a given measurement
    tag_keys = client.get_tag_values(tag_key=TEST_TAG_KEY_A)
    assert set(tag_keys) == {TEST_TAG_VALUE_A, FOO}

    # The client should still return the tag values of a given measurement
    tag_keys = client.get_tag_values(
        tag_key=TEST_TAG_KEY_A, measurement=TEST_MEASUREMENT
    )
    assert set(tag_keys) == {TEST_TAG_VALUE_A}

    # The client should return empty data and raise a warning
    # when the given tag_key and/or measurement does not exist
    with pytest.warns(UserWarning):
        assert len(client.get_tag_values(tag_key=DOES_NOT_EXIST)) == 0
    with pytest.warns(UserWarning):
        tag_values = client.get_tag_values(
            tag_key=TEST_TAG_KEY_A, measurement=DOES_NOT_EXIST
        )
        assert len(tag_values) == 0
    with pytest.warns(UserWarning):
        tag_values = client.get_tag_values(
            tag_key=DOES_NOT_EXIST, measurement=TEST_MEASUREMENT
        )
        assert len(tag_values) == 0


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_delete_measurement(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test deleting points from the InfluxDB using measurement
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # Deleting with a missing measurement should raise an error
    with pytest.raises(errors.SchemaError):
        client.delete(measurement=DOES_NOT_EXIST)

    # Test deleting all data from a measurement
    client.delete(measurement=TEST_MEASUREMENT)
    df = client.read_data_frame(measurement=TEST_MEASUREMENT, start=INFLUX_EPOCH)
    assert df.empty

    # Deleting without specifying measurement and tags is not possible
    # and should raise an error
    # Deleting with a missing measurement should raise an error
    with pytest.raises(errors.DeleteError):
        client.delete()


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_delete_tags(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test deleting points from the InfluxDB using tags
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client: Client = request.getfixturevalue(client_func)

    # Write new data with a new tag
    new_tags = {"c": "foo"}
    all_tags = dict(TEST_TAGS, **new_tags)
    new_data = pd.DataFrame(
        data={QUX: (1, 2)}, index=pd.date_range(TEST_DATA_START, periods=2)
    )
    client.write_data_frame(
        data_frame=new_data.copy(deep=True),
        measurement=TEST_MEASUREMENT,
        tags=new_tags,
    )

    # Deleting with a missing tag key/value pair should raise a warning
    with pytest.warns(UserWarning):
        client.delete(
            measurement=TEST_MEASUREMENT, tags={DOES_NOT_EXIST: DOES_NOT_EXIST}
        )

    # Deleting the 'a': 'crux' tag key/value pair should leave only the new data
    # in the measurement
    client.delete(measurement=TEST_MEASUREMENT, tags=TEST_TAGS)
    df = client.read_data_frame(measurement=TEST_MEASUREMENT, start=INFLUX_EPOCH)
    pandas.testing.assert_frame_equal(
        left=df, right=new_data, check_like=True, check_freq=False
    )

    # Deleting with exclusive tag key/value pairs should should raise a warning
    with pytest.warns(UserWarning):
        client.delete(measurement=TEST_MEASUREMENT, tags=all_tags)

    # Deleting the new tag without giving a measurement should delete all data in the
    # test measurement
    client.delete(tags=new_tags)
    df = client.read_data_frame(measurement=TEST_MEASUREMENT, start=INFLUX_EPOCH)
    assert df.empty


# endregion


# region Read Data
@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_read_data_frame(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test reading the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = TEST

    # Test reading the entire data
    start = TEST_DF.index.min()
    df = client.read_data_frame(measurement=test_measurement, start=start)
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )

    # Test filtering for tags
    # Querying for all tags should yield the test data
    df = client.read_data_frame(
        measurement=test_measurement, start=start, tags=TEST_TAGS
    )
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )
    # Querying for a single tag should yield the test data
    query_tag_key = TEST_TAG_KEYS[0]
    query_tag = {query_tag_key: TEST_TAGS[query_tag_key]}
    df = client.read_data_frame(
        measurement=test_measurement, start=start, tags=query_tag
    )
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )
    query_tag_key = TEST_TAG_KEYS[1]
    query_tag = {query_tag_key: TEST_TAGS[query_tag_key]}
    df = client.read_data_frame(
        measurement=test_measurement, start=start, tags=query_tag
    )
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )
    # Querying for a non-existing tag should yield no data
    df = client.read_data_frame(
        measurement=test_measurement, start=start, tags={"a": "does not exist"}
    )
    assert df.empty

    # Test reading the entire data with explicitly given fields
    start = TEST_DF.index.min()
    df = client.read_data_frame(
        measurement=test_measurement, fields=tuple(TEST_DATA.keys()), start=start
    )
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )

    # Test reading a fields subset
    fields = list(TEST_DATA.keys())[0:2]
    start = TEST_DF.index.min()
    df = client.read_data_frame(
        measurement=test_measurement, fields=fields, start=start
    )
    expected = TEST_DF[fields]
    pandas.testing.assert_frame_equal(
        left=df, right=expected, check_like=True, check_freq=False
    )

    # Limiting the results with the stop parameter should return the
    # first entry of the test data
    start = TEST_DF.index.min()
    stop = TEST_DF.index.max() - pd.Timedelta("1h")
    df = client.read_data_frame(
        measurement=test_measurement,
        fields=tuple(TEST_DATA.keys()),
        start=start,
        stop=stop,
    )
    expected = TEST_DF.iloc[[0]]
    pandas.testing.assert_frame_equal(
        left=df, right=expected, check_like=True, check_freq=False
    )

    # Limiting the results with the limit parameter should return the
    # first entry of the test data
    start = TEST_DF.index.min()
    df = client.read_data_frame(
        measurement=test_measurement, start=start, limit=1
    )
    expected = TEST_DF.iloc[[0]]
    pandas.testing.assert_frame_equal(
        left=df, right=expected, check_like=True, check_freq=False
    )


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_read_first_point(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test reading the first point from a field from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = TEST
    field = TEST_DF.columns[0]

    # Test reading the first value from the first test data field
    start = TEST_DF.index.min()
    results = client.read_first_point(
        measurement=test_measurement, field=field, start=start
    )
    assert len(results.index) == 1  # Check this criterion once to be sure
    assert results.iloc[0] == TEST_DF[field].iloc[0]

    # Test reading the first value from the first test data field
    # with a later start criterion
    start = TEST_DF.index.min() + pd.Timedelta("1h")
    results = client.read_first_point(
        measurement=test_measurement, field=field, start=start
    )
    assert results.iloc[0] == TEST_DF[field].iloc[1]

    # If there are no points in the given date-time range,
    # the method should return an empty series
    start = TEST_DF.index.max() + pd.Timedelta("1h")
    results = client.read_first_point(
        measurement=test_measurement, field=field, start=start
    )
    assert results.empty


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_read_last_point(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test reading the last point from a field from the InfluxDB
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = TEST
    field = TEST_DF.columns[0]

    # Test reading the last value from the first test data field
    start = TEST_DF.index.min()
    results = client.read_last_point(
        measurement=test_measurement, field=field, start=start
    )
    assert results.iloc[-1] == TEST_DF[field].iloc[-1]

    # Test reading the last value from the first test data field
    # with an earlier stop criterion
    start = TEST_DF.index.min()
    stop = start + pd.Timedelta("1h")
    results = client.read_last_point(
        measurement=test_measurement, field=field, start=start, stop=stop
    )
    assert results.iloc[-1] == TEST_DF[field].iloc[0]

    # If there are no points in the given date-time range,
    # the method should return an empty series
    start = TEST_DF.index.max() + pd.Timedelta("1h")
    results = client.read_last_point(
        measurement=test_measurement, field=field, start=start
    )
    assert results.empty


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_poll_field_static(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest
):
    """
    Test polling for data using already available data
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = TEST
    field = TEST_DF.columns[0]

    # Polling a single exact value giving start and return_first
    # should return a one-point series
    # Valid range: (-------]
    # Points:      ^          ^
    start = TEST_DF.index.min()
    results = client.poll_field(
        measurement=test_measurement, field=field, start=start, return_first=True
    )
    assert len(results.index) == 1
    assert results.iloc[0] == TEST_DF[field][start]

    # Poll a single value with validity period giving start and return_first
    # Valid range: ****(---****]
    # Points:        ^            ^
    start = TEST_DF.index.min() + pd.Timedelta("1h")
    validity_period = pd.Timedelta("2h")
    results = client.poll_field(
        measurement=test_measurement,
        field=field,
        start=start,
        validity_period=validity_period,
        return_first=True,
    )
    assert len(results.index) == 1
    assert results.iloc[0] == TEST_DF[field].iloc[0]

    # Poll a single value with validity period giving start and stop
    # Valid range: ****(*******]
    # Points:         ^           ^
    start = TEST_DF.index.min() + pd.Timedelta("1h")
    stop = start + pd.Timedelta("1h")
    validity_period = pd.Timedelta("2d")
    results = client.poll_field(
        measurement=test_measurement,
        field=field,
        start=start,
        stop=stop,
        validity_period=validity_period,
    )
    assert len(results.index) == 1
    assert results.iloc[0] == TEST_DF[field].iloc[0]

    # Poll an exact series with stop criterion using stop_is_inclusive
    # Valid range: (-------)
    # Points:      ^       ^
    start = TEST_DF.index[0]
    stop = TEST_DF.index[1]
    results = client.poll_field(
        measurement=test_measurement,
        field=field,
        start=start,
        stop=stop,
        stop_is_inclusive=True,
    )
    pandas.testing.assert_series_equal(
        left=results, right=TEST_DF[field].iloc[0:2], check_freq=False
    )

    # Poll an exact series with stop criterion using validity period
    # Valid range: (-------)
    # Points:      ^       ^
    start = TEST_DF.index[0]
    stop = TEST_DF.index[1]
    validity_period = pandas.Timedelta("0s")
    results = client.poll_field(
        measurement=test_measurement,
        field=field,
        start=start,
        stop=stop,
        validity_period=validity_period,
    )
    pandas.testing.assert_series_equal(
        left=results, right=TEST_DF[field].iloc[0:2], check_freq=False
    )

    # Poll a series with stop criterion and validity period
    # Valid range: **(----****)
    # Points:      ^      ^
    start = TEST_DF.index.min() + pd.Timedelta("1h")
    stop = TEST_DF.index.max() + pd.Timedelta("2h")
    validity_period = pd.Timedelta("2h")
    results = client.poll_field(
        measurement=test_measurement,
        field=field,
        start=start,
        stop=stop,
        validity_period=validity_period,
    )
    pandas.testing.assert_series_equal(
        left=results, right=TEST_DF[field].iloc[0:2], check_freq=False
    )

    # Test timeout with non-existing data
    # Valid range:     (--------)
    # Points:      ^              ^
    start = TEST_DF.index.min() + pd.Timedelta("2h")
    stop = TEST_DF.index.max() - pd.Timedelta("2h")
    with pytest.raises(errors.PollingError):
        client.poll_field(
            measurement=test_measurement,
            field=field,
            start=start,
            stop=stop,
            timeout=pd.Timedelta("1ms"),
        )

    # Test timeout with non-existing data using a validity period
    # Valid range:   **(----****)
    # Points:      ^              ^
    start = TEST_DF.index.min() + pd.Timedelta("2h")
    stop = TEST_DF.index.max() - pd.Timedelta("2h")
    validity_period = pd.Timedelta("1h")
    with pytest.raises(errors.PollingError):
        client.poll_field(
            measurement=test_measurement,
            field=field,
            start=start,
            stop=stop,
            validity_period=validity_period,
            timeout=pd.Timedelta("1ms"),
        )

    # Giving timezone-naive range should raise an error
    start = TEST_DF.index.min() + pd.Timedelta("2h")
    start.replace(tzinfo=None)
    with pytest.raises(errors.PollingError):
        client.poll_field(
            measurement=test_measurement,
            field=field,
            start=start,
        )


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_poll_field_dynamic(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest, caplog
):
    """
    Test polling for data while concurrently creating data
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    :param caplog: pytest's built-in log capturing fixture
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = FOO
    field = TEST_DF.columns[0]
    caplog.set_level(logging.DEBUG, logger=LOGGER_NAME)

    # Create test data in the DB
    def create_data():
        """
        [Thread] Wait a second, then write test data
        """
        time.sleep(1)
        client.write_data_frame(
            measurement=test_measurement,
            data_frame=TEST_DF,
        )

    # Poll an exact series with stop criterion
    # Valid range: (-------)
    # Points:      ^       ^
    start = TEST_DF.index[0]
    stop = TEST_DF.index[1]
    writing_thread = threading.Thread(target=create_data)
    writing_thread.start()
    results = client.poll_field(
        measurement=test_measurement,
        field=field,
        start=start,
        stop=stop,
        stop_is_inclusive=True,
        sleep_time_ms=500,
    )
    writing_thread.join()
    pandas.testing.assert_series_equal(
        left=results, right=TEST_DF[field].iloc[0:2], check_freq=False
    )
    # Assert the client has been sleeping in the start point polling phase
    # from logging records
    sleep_records = [
        record
        for record in caplog.record_tuples
        if "No valid start point available" in record[2]
    ]
    assert len(sleep_records) > 0


@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_poll_field_dynamic_timeout(
    db_init_func: str, client_func: str, request: pytest.FixtureRequest, caplog
):
    """
    Test the timeout while polling for data while concurrently creating data
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    :param caplog: pytest's built-in log capturing fixture
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = FOO
    field = TEST_DF.columns[0]
    caplog.set_level(logging.DEBUG, logger=LOGGER_NAME)

    # Create test data in the DB
    def create_data():
        """
        [Thread] Wait a second, then write test data
        """
        time.sleep(1)
        client.write_data_frame(
            measurement=test_measurement,
            data_frame=TEST_DF,
        )

    # Poll an exact series with stop criterion with timeout
    # Valid range: (-------)
    # Points:      ^       ^
    start = TEST_DF.index[0]
    stop = TEST_DF.index[1]
    writing_thread = threading.Thread(target=create_data)
    writing_thread.start()
    with pytest.raises(errors.PollingError):
        client.poll_field(
            measurement=test_measurement,
            field=field,
            start=start,
            stop=stop,
            stop_is_inclusive=True,
            sleep_time_ms=100,
            timeout=pd.Timedelta("200ms"),
        )
    writing_thread.join()

    # Assert the client has been sleeping in the start point polling phase
    # from logging records
    sleep_records = [
        record
        for record in caplog.record_tuples
        if "No valid start point available" in record[2]
    ]
    assert len(sleep_records) > 0


# endregion


# region Write Data
SUPPORTED_WRITE_MODES = (WriteMode.synchronous.value, WriteMode.batching.value)


@pytest.mark.parametrize("write_mode", SUPPORTED_WRITE_MODES)
@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_write_data_frame(
    db_init_func: str,
    client_func: str,
    write_mode: WriteMode,
    request: pytest.FixtureRequest,
):
    """
    Test writing to the InfluxDB in synchronous mode
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = FOO

    # Test writing test data to the DB
    client.write_data_frame(
        data_frame=TEST_DF, measurement=test_measurement, write_mode=write_mode
    )
    start = TEST_DF.index.min()
    df = client.read_data_frame(measurement=test_measurement, start=start)
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )

    # Test writing data with tags
    client.write_data_frame(
        data_frame=TEST_DF.copy(),
        measurement=test_measurement,
        tags=TEST_TAGS,
        write_mode=write_mode,
    )
    # Querying for a single tag should yield the test data
    start = TEST_DF.index.min()
    query_tag_key = TEST_TAG_KEYS[0]
    query_tag = {query_tag_key: TEST_TAGS[query_tag_key]}
    df = client.read_data_frame(
        measurement=test_measurement, start=start, tags=query_tag
    )
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )
    query_tag_key = TEST_TAG_KEYS[1]
    query_tag = {query_tag_key: TEST_TAGS[query_tag_key]}
    df = client.read_data_frame(
        measurement=test_measurement, start=start, tags=query_tag
    )
    pandas.testing.assert_frame_equal(
        left=df, right=TEST_DF, check_like=True, check_freq=False
    )


@pytest.mark.parametrize("write_mode", SUPPORTED_WRITE_MODES)
@pytest.mark.parametrize("db_init_func,client_func", VERSION_SETUPS_V1_V2)
def test_write_data_frame_much_data(
    db_init_func: str,
    client_func: str,
    write_mode: WriteMode,
    request: pytest.FixtureRequest,
):
    """
    Test writing to the InfluxDB in synchronous mode
    :param db_init_func: [Fixture Name] A test InfluxDB with initial data
    :param client_func: [Fixture Name] The client under test
    """
    request.getfixturevalue(db_init_func)
    client = request.getfixturevalue(client_func)

    test_measurement = FOO

    df = pd.DataFrame(
        np.random.randint(0, 100, size=(100, 50)),
        index=pd.date_range(TEST_DATA_START, periods=100),
        columns=range(50),
    )
    df.columns = df.columns.astype(str)
    client.write_data_frame(
        data_frame=df, measurement=test_measurement, write_mode=write_mode
    )
    start = df.index.min()
    df_ = client.read_data_frame(measurement=test_measurement, start=start)
    pandas.testing.assert_frame_equal(
        left=df,
        right=df_,
        check_like=True,
    )


# endregion
