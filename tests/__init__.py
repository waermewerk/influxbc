import os

INFLUX_HOST = os.environ.get("IBC_INFLUX_HOST") or "localhost"
INFLUX_V1_PORT = int(os.environ.get("IBC_INFLUX_V2_PORT", 0)) or 8086
INFLUX_V2_PORT = int(os.environ.get("IBC_INFLUX_V2_PORT", 0)) or 8087
INFLUX_ORG = os.environ.get("IBC_INFLUX_ORG") or "ibc_tests"
INFLUX_DB = os.environ.get("IBC_INFLUX_DB") or "ibc_tests"
INFLUX_USER = os.environ.get("IBC_INFLUX_USER") or "ibc_tests"
INFLUX_PW = os.environ.get("IBC_INFLUX_PW") or "ibc_tests"
INFLUX_TOKEN = os.environ.get("IBC_INFLUX_TOKEN") or "ibc_tests"

TEST = "test"
FOO = "foo"
BAR = "bar"
BAZ = "baz"
QUX = "qux"
DOES_NOT_EXIST = "DOES_NOT_EXIST"
