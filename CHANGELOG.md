## 0.1.3

### Changes

* [Logging] Get messages with HTTP sent requests from both clients

### Fixes

* [V1] read_first_point, read_last_point, poll_field could not handle field keys with non-ASCII characters
* [Logging] Default level was too verbose

## 0.1.2

This was the initial release.
